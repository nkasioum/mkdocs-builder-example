FROM gitlab-registry.cern.ch/authoring/documentation/mkdocs:stable
WORKDIR /opt/
COPY docs/ ./docs/
COPY mkdocs.yml .
RUN mkdocs build --clean --site-dir public
EXPOSE 8000
CMD ["python", "-m", "http.server", "--directory", "public"]
